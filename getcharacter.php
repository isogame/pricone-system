<?php

// プリコネシステム データインポート用プログラム ver1.00
// GameWith の {キャラクター}名の評価と専用装備 のURL を source_url に格納して analyzecharacter に投げるといい感じにデータベースにインポートしてくれる。
//
// 引数に与えるURLはここでみつける。
// 全キャラ評価一覧
// https://gamewith.jp/pricone-re/article/show/92923
//
// Source URLの例
// ペコリーヌ→ https://gamewith.jp/pricone-re/article/show/92887
// バレンタインシズル→ https://gamewith.jp/pricone-re/article/show/141208

//引数からURLを渡したときの処理
if (isset($_GET['url'])) { //URL引数をチェック
    $source_url = $_GET['url'];
} elseif (isset($argv[1])) { //コマンドライン引数をチェック
    $source_url = $argv[1];
} else { //なんにも渡さずに実行するとバレンタインシズルをチェックしちゃう。
    $source_url = "https://gamewith.jp/pricone-re/article/show/141208";
}

//開始
echo "<p>getcharacter.php: {$source_url} からキャラクターの情報と必要装備、素材の情報をダウンロードします...</p>".PHP_EOL;

//MySQL接続
require_once("database_connector.php");
$db_session = dbconnect();

analyzecharacter(getcontents($source_url));

//analyzepartsdetail(getcontents("https://gamewith.jp/pricone-re/article/show/135105"),"");

//MySQL接続解除
dbdisconnect($db_session);

echo "<BR>".PHP_EOL;
echo "<p>getcharacter.php: すべての処理が完了しました。</p>".PHP_EOL;

//GameWith あるいは キャッシュ情報からデータをロードするための汎用関数
function getcontents($source_url)
{
    echo "<p>" . PHP_EOL;

    //入力はURL。この関数でキャッシュからHTMLをロードするのか、URLをもとにHTTPサーバーからダウンロードしてロードするのか判定して、どちらにせよ呼び出し元にHTMLコンテンツを返す。
    //HTTPサーバーからコンテンツを取得した場合はキャッシュディレクトリに保存する。
    echo "{$source_url} を処理しています...<BR>" . PHP_EOL;

    //URLの末尾の番号を取得してキャッシュ済みのファイルと照合する準備
    preg_match('/.*\/(.*)$/i', $source_url, $filename);
    
    // $source_page に処理対象のファイルをロード。cache ディレクトリにキャッシュ済みのファイルがあれば、そちらを優先する。
    if (file_exists("cache/{$filename[1]}")) {
        //ファイルがキャッシュに存在し、キャッシュのデータをロードする場合
        $source_page = file_get_contents("cache/{$filename[1]}");

        //ファイルの内容が空である場合は、キャッシュファイル破損とみなして再試行を行う。getcontentsに与えたURLがそもそも間違っているとかで発生しうる。
        if ($source_page != "") {
            echo "-> ローカルファイル {$filename[1]} にキャッシュされた HTMLコンテンツ を検出しました。ロードしました。<BR>" . PHP_EOL;
        } else {
            echo "-> ローカルファイル {$filename[1]} にキャッシュされた HTMLコンテンツ が検出されましたが、破損していることを検出しました。<BR>" . PHP_EOL;
            //キャッシュファイルを削除する。
            unlink("cache/{$filename[1]}");
            //再試行する。
            getcontents($source_page);
        }
    } else {
        //ファイルがキャッシュに存在せず、HTTP通信を行ってデータをダウンロードする場合。
        echo "-> キャッシュされたコンテンツがありません。URL ".$source_url." からダウンロードを開始します...<BR>" . PHP_EOL;

        //gamewith.jp に負荷がかかりすぎないようにこの関数でHTTPダウンロードするときはWaitを設ける
        sleep(2);

        $source_page = file_get_contents($source_url);

        //データの内容が空である場合は、キャッシュファイルとして保存しない。URL文字列の解析エラーかもしれないので、安全性をとって再試行は自動的に行わずスキップする。
        if ($source_page != "") {
            echo "-> ダウンロードを完了しました。ロードしました。<BR>" . PHP_EOL;
            //キャッシュにデータ保存。
            file_put_contents("cache/{$filename[1]}", $source_page);
            echo "-> キャッシュ ファイルを保存しました: {$filename[1]}<BR>" . PHP_EOL;
        } else {
            echo "-> ダウンロード失敗しました。データをロードできません。<BR>" . PHP_EOL;
        }
    }

    echo "</p>" . PHP_EOL;
    //どちらかの方法でゲットしたHTMLコンテンツをreturnで返す。
    return $source_page;
}

//キャラクター情報
function analyzecharacter($source_page)
{
    //MySQL接続した情報を引き継ぐ
    global $db_session,$character_id,$parts_rank;

    //入力はHTMLコンテンツ。キャラクター名を検出する。

    //preg_match('/【プリコネR】(?P<charactername>.+)の評価/', '【プリコネR】ペコリーヌの評価と専用装備 |星6ペコリーヌの性能/絆ボナ【プリンセスコネクト】', $source_page, $temp);
    //検出対象の文字列はHEAD→TITLEタグ内の右に示すような文字列: 【プリコネR】ペコリーヌの評価と専用装備 |星6ペコリーヌの性能/絆ボナ【プリンセスコネクト】
    preg_match('/【プリコネR】(?P<charactername>.+)の評価/', $source_page, $temp);
    //var_dump($temp);

    echo "<p>" . PHP_EOL;

    //キャラクター名の検出
    if ($temp['charactername'] !== '') {
        echo "キャラクター名を検出しました: {$temp['charactername']}。<BR>" . PHP_EOL;
        $character_name = $temp['charactername'];

        //キャラクターのIDを抽出
        preg_match('/rectangle\/(?P<characterid>\d+).png" alt/', $source_page, $temp);
        $character_id = $temp['characterid'];

        //検出したキャラクター名をデータベースに書き込む
        $sqlquery = "INSERT INTO characters (ID,Name) VALUES ('{$character_id}','{$character_name}')";
        if (!$result = $db_session->query($sqlquery)) {
            echo "ERROR: キャラクターが既に存在しているか、なんらかのデータベースエラーによりキャラクターの情報は学習されませんでした。<BR>".PHP_EOL;
        }
    } else {
        echo "ERROR: キャラクター名の検出に失敗しました。<BR>" . PHP_EOL;
    }

    //装備一覧の抽出。抽出対象の装備数をカウントする。
    $parts_articles = substr_count($source_page, 'article/show/');
    echo $character_name . " の装備を " . $parts_articles . "個検出しました。" . PHP_EOL;

    //装備を6つカウントするごとにレベルがあがる。最初はレベル1からスタート。
    $parts_rank = 1;
    $parts_count = 0;
    //「〜の要求装備」と書いてある行だけ抽出したい。後半の「装備/ランクアップ関連記事はこちら」の手前まで切り出したいので「ランクアップ関連記事はこちら」で切っている。
    preg_match('/の要求装備<\/h3>(.*?)ランクアップ関連記事はこちら/', $source_page, $temp);

    //var_dump($temp);
    //装備アイコンとそのアイコンをクリックした時に飛ぶ先のリンクURLを取得する
    preg_match_all("/<a href=\'(.*?)\'\s><img src=\'(.*?)\s\' width=\'33\' \/><\/a>/", $temp[1], $parts_list);

    //先頭行に抽出されてしまうゴミを削除
    //array_splice($parts_list[1], 0, 1);
    //array_splice($parts_list[2], 0, 1);
    //正規表現で一致する前のなにかを削除（しなくてもいい。var_dumpではきだすときに若干見栄えがいいとかそういう問題）
    unset($parts_list[0]);

    echo "</p>" . PHP_EOL;


    //装備の詳細情報へのリンク一覧[1]と画像一覧[2]の二次元配列
    //var_dump($parts_list);
    echo "====== RANK {$parts_rank} ======<BR>".PHP_EOL;

    foreach (array_map(null, $parts_list[1], $parts_list[2]) as [$partslink, $imgurl]) {
        //echo "$partslink $imgurl".PHP_EOL;

        //analyzepartsdetail ファンクションに「XXXXX装備の入手方法と素材ドロップ場所一覧」のHTMLコンテンツ本体と、装備の画像ファイルURLを渡すとよしなにしてくれる
        analyzepartsdetail(getcontents($partslink), $imgurl, 1);

        $parts_count++;
        if ($parts_count == 6) {
            $parts_rank++;
            $parts_count = 0;
            echo "====== RANK {$parts_rank} ======".PHP_EOL;
        }
    }
}

//元々要求されているこの装備がいくついるのかって情報を書いて、取得した作成に必要な素材数x渡した必要作成数とする必要があるかもしれないので $quantity を実装してあるが、いらないかもしれない。
function analyzepartsdetail($source_page, $imgurl, $quantity)
{
    //MySQL接続した情報を引き継ぐ
    global $db_session,$character_id,$parts_rank;

    //e.x) 「アイアンブレード」の入手方法と素材ドロップ場所一覧　とかタイトルに書いてあるところの括弧書きの中から装備名を取得
    preg_match('/「(?P<partsname>.+)」の入手方法/', $source_page, $temp);

    //装備名の検出
    if ($temp['partsname'] !== '') {
        echo "装備名を検出しました: {$temp['partsname']}。<BR>" . PHP_EOL;
        $parts_name = $temp['partsname'];
    } else {
        echo "ERROR: 装備名の検出に失敗しました。<BR>" . PHP_EOL;
    }

    //装備のIDを抽出
    preg_match('/article\/show\/(?P<parts_id>\d+)">/', $source_page, $temp);
    $parts_id = $temp['parts_id'];

    //検出した装備名をデータベースに書き込む
    $sqlquery = "INSERT INTO equipments (ID,Name) VALUES ('{$parts_id}','{$parts_name}')";
    if (!$result = $db_session->query($sqlquery)) {
        echo "ERROR: 装備が既に存在しているか、なんらかのデータベースエラーによりこの装備の情報は学習されませんでした。<BR>".PHP_EOL;
    }

    //装備を示す画像ファイルの取得
    preg_match('/.*\/(.*)$/i', $imgurl, $filename);
    if (!file_exists('img/'.$filename[1])) {
        echo "{$parts_name} の画像ファイル({$filename[1]})が ローカルストア に見つかりませんでした。ダウンロードします...<BR>" . PHP_EOL;
        sleep(2);
        file_put_contents('img/'.$filename[1], file_get_contents($imgurl));
    }
    
    //装備の作成に必要な素材の検出
    preg_match('/<h3>作成に必要な素材(.*?)<\/div><h3>/', $source_page, $temp);

    //素材が設計図である場合（リンク先がない場合）のサンプル
    //<td><img src='https://gamewith.akamaized.net/assets/images/common/transparent1px.png' data-original='https://gamewith.akamaized.net/article_tools/pricone-re/gacha/44_s.png' data-use-lazyload='true' width=50px height=50px class='js-lazyload-fixed-size-img c-blank-img w-article-img' ><noscript><img src='https://gamewith.akamaized.net/article_tools/pricone-re/gacha/44_s.png'  /></noscript>レギオンヘルムの設計図×1</td>
    
    //素材が装備である場合（リンク先がある場合）のサンプル
    //<td><a href='https://gamewith.jp/pricone-re/article/show/99149' ><img src='https://gamewith.akamaized.net/article_tools/pricone-re/gacha/98_s.png ' width='50' />スケイルメイル×1</a></td>
        
    // 素材 or 装備ごとに <td>.....素材or装備の情報.....</td> でかこまれている。

    // 素材名と必要個数の取得　　e.x)レギオンヘルムの設計図×1
    // 素材系の場合は <img src='xxxx' width='50' >装備名×1</a>　となっているが、設計図系の場合 <noscript>設計図名×1</noscript> となっており、共通点が >名前×個数< しかない。
    // したがって、正規表現での抽出配下のように行う....これでいいのか？
    // <td>.*>(.*?)×(¥d)<.*<\/td>
    if (!$temp[1] == "") {
        echo "{$parts_name} の作成に必要な素材を検出しています...<BR>" . PHP_EOL;
        preg_match_all('/(?:<noscript><img src=\'|\'\s><img src=\')(.*?)(?:\s?)\'\s(?:width=\'50\'\s\/|\s\/><\/noscript)>(.*?)(?:<\/a>|<\/td>)/', $temp[1], $parts_temp);
        unset($parts_temp[0]);
        $parts_count = 0;
        foreach (array_map(null, $parts_temp[1], $parts_temp[2]) as [$op_imgurl, $op_parts_name]) {
            //echo "{$op_imgurl}: {$op_parts_name}".PHP_EOL;

            // 装備or素材名と必要個数を分離
            preg_match('/(.*)×(\d*)/', $op_parts_name, $op_parts_name);
            echo "-> {$op_parts_name[1]} ×{$op_parts_name[2]}<BR>".PHP_EOL;
            //var_dump($op_parts_name);

            //装備or素材名を示す画像ファイルの取得
            preg_match('/.*\/(.*)$/i', $op_imgurl, $filename);
            if (!file_exists('img/'.$filename[1])) {
                echo "{$op_parts_name[1]} の画像ファイル({$filename[1]})が ローカルストア に見つかりませんでした。ダウンロードします...<BR>" . PHP_EOL;
                sleep(2);
                file_put_contents('img/'.$filename[1], file_get_contents($imgurl));
            }

            //装備or素材のIDを修得（画像ファイル名から）
            preg_match('/(\d*)_s.(?:png|jpg)/', $filename[1], $op_parts_id);
            $op_parts_id = $op_parts_id[1];

            //検出した素材名をデータベースに書き込む
            $sqlquery = "INSERT INTO materials (ID,Name) VALUES ('{$op_parts_id}','{$op_parts_name[1]}')";
            if (!$result = $db_session->query($sqlquery)) {
                echo "ERROR: 素材が既に存在しているか、なんらかのデータベースエラーによりこの素材の情報は学習されませんでした。<BR>".PHP_EOL;
            }
            
            //必要素材数をデータベースに書き込む（ここがこのシステムの最重要部分）
            $parts_quantity_calc = $op_parts_name[2]*$quantity;
            $sqlquery = "INSERT INTO parts (CharacterID,Rank,EquipmentID,MaterialID,Quantity) VALUES ('{$character_id}','{$parts_rank}','{$parts_id}','{$op_parts_id}','{$parts_quantity_calc}')";
            //cho $sqlquery;
            if (!$result = $db_session->query($sqlquery)) {
                echo "ERROR: 必要素材数データが既に存在しているか、なんらかのデータベースエラーにより学習されませんでした。<BR>".PHP_EOL;
            }

            //この装備を作るために必要な素材 or 装備の数を数えている。
            $parts_count++;
        }
        
        //装備のリンク先だけじゃなくて画像ファイル名も欲しいなと思って preg_match_all を書き直したのでボツにしたプログラム。
        //こちらのほうがシンプルなので、どっかで原点回帰しようと思ったらいるかもしれない。
        //preg_match_all('/(?:\'50\'\s\/|\/noscript)>(.*?)(?:<\/a>|<\/td>)/', $temp[1], $parts_temp);
        //$parts_temp = array_splice($parts_temp, 1, 1);
        //$parts_count = count($parts_temp,COUNT_RECURSIVE)-1;

        echo "{$parts_name} の作成に必要な素材を {$parts_count}個　検出: <BR>" . PHP_EOL;

        //var_dump($parts_temp);

        // レギオンヘルムでいうところの「スケイルメイル」のように、素材を作るのにさらに素材が必要なケースがあるならば、再帰検索を行う必要があるので、素材名にリンクが設定されているか否かで判断する。
        echo "{$parts_name} の作成に必要な素材に、さらに追加で素材を要求するものがないか検索しています........<BR>" . PHP_EOL;
        preg_match_all("/<a href=\'(.*?)\'\s><img src=\'(.*?)\s\' width=\'50\' \/>(.*?)×(.*?)<\/a>/", $temp[1], $option_parts_list);

        // 検索にヒットした場合は、Array [0] HTMLソース全体、[1]素材の詳細ページURL [2]素材の画像URL [3]素材名 が入ってくる。
        // 2つ以上追加で検索が必要な素材がヒットした場合は、[0] HTMLソース全体、[1]素材の詳細ページURL [2]素材の画像URL [3]素材名 [4]素材の詳細ページURL...となるはず。
        // [0] HTMLソース全体 は不要な情報なので消す。
        unset($option_parts_list[0]);
        // あとは1素材につき3要素ずつArrayが作られる。
        // でも 素材1のURL、素材2のURL、素材1の画像、素材2の画像、みたいになってしまって処理するのに都合が悪いので、array_map を実行する。
        // 結果 素材1のURL、素材1の画像、素材2のURL、素材2の画像、となるので処理しやすい。
        
        $option_parts_list = array_map(null, $option_parts_list[1], $option_parts_list[2], $option_parts_list[3], $option_parts_list[4]);

        //処理すべきオプションパーツの数を数える
        $option_parts_count = count($option_parts_list);

        if ($option_parts_count >= 1) {
            //オプションパーツが1つ以上検出された場合は再帰検索を開始する。次の装備のデータをダウンロードしはじめるのは再帰検索がすべて終了してから。
            echo "オプションパーツを {$option_parts_count}個 検出しました:<BR>" . PHP_EOL;
            var_dump($option_parts_list);
            foreach ($option_parts_list as [$op_partslink, $op_imgurl, $op_parts_name, $op_parts_quantity]) {
                echo "{$op_parts_name} の装備情報について調べています...<BR>" . PHP_EOL;
                analyzepartsdetail(getcontents($op_partslink), $op_imgurl, $op_parts_quantity);
            }
        } else {
            echo "オプションパーツは検出されませんでした。" . PHP_EOL;
        }
    } else {
        echo "{$parts_name} の作成に必要な素材は検出されませんでした。<BR>" . PHP_EOL;
    }
    
    echo "<BR>" . PHP_EOL;
}
