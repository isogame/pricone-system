<?php
require_once("database_settings.php");

function dbconnect()
{
    $db_session = @new mysqli(connect_dbsvr, connect_dbusr, connect_dbpwd, connect_dbnam);
    if ($db_session->connect_error) {
        echo "error";
        exit();
    }
    $db_session->set_charset('utf8');
    return $db_session;
}

function dbdisconnect($db_session)
{
    $db_session->close();
}
